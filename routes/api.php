<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('flux_finances/export/', 'API\FluxFinanceController@export');
Route::middleware('auth:api')->get('locations/export/', 'API\LocationController@export');

Route::middleware('auth:api')->get('dispatcher/options/{page}', 'API\OptionDispatcherController@get');
Route::middleware('auth:api')->post('locations/prolongement/', 'API\ProlongementController@store');

// Route::middleware('auth:api')->post('projets/', 'API\ProjetController@index'); 

Route::middleware('auth:api')->post('finances/rapports/voiture/par-annee', 'API\RapportFinanceController@par_annee');


Route::middleware('auth:api')->get('cartes-grises/{id}/download', 'API\CarteGriseController@getFile');
Route::middleware('auth:api')->get('assurances/{id}/download', 'API\AssuranceController@getFile');
Route::middleware('auth:api')->get('visites-techniques/{id}/download', 'API\VisiteTechniqueController@getFile');


//Route pour modifier les # parties du véhicule
Route::middleware('auth:api')->patch('vehicules/{id}/base', 'API\VehiculeController@updateBase');
Route::middleware('auth:api')->patch('vehicules/{id}/dimensions', 'API\VehiculeController@updateDimensions');
Route::middleware('auth:api')->patch('vehicules/{id}/updateOptions', 'API\VehiculeController@updateOptions');

Route::middleware('auth:api')->patch('vehicules/{id}/updateImages', 'API\VehiculeImageController@updateImages');

Route::middleware('auth:api')->get('profil', 'API\ProfilController@index');
Route::middleware('auth:api')->get('profil/activites', 'API\ProfilController@activities');

Route::middleware('auth:api')->patch('profil/password', 'API\ProfilController@password');
Route::middleware('auth:api')->patch('profil/info', 'API\ProfilController@info');
Route::middleware('auth:api')->patch('profil/avatar', 'API\ProfilController@avatar');

Route::middleware('auth:api')->get('historiques', 'API\HistoriqueController@index');
Route::middleware('auth:api')->get('recherche', 'API\SearchController@search');
Route::middleware('auth:api')->get('graphs/chiffre_affaire', 'API\GraphController@chiffre_affaire_graph');
Route::middleware('auth:api')->get('/entreprises/enterpriseAsParameter', 'API\EntrepriseController@getAsParams');
Route::middleware('auth:api')->get('/membres/memberAsParameter', 'API\MembreController@getAsParams');

Route::middleware('auth:api')->get('/projets/projetAsParameter', 'API\ProjetController@getAsParams');

Route::group(['middleware' => 'auth:api'], function()
{
    Route::apiResources(
        [
            'utilisateurs'=>"API\UserController",
            'assurances'=>"API\AssuranceController",
            'cartes-grises'=>"API\CarteGriseController",
            'visites-techniques'=>"API\VisiteTechniqueController",
            'roles'=>"API\RoleController",
            'membres'=>"API\MembreController",
            'projets'=>"API\ProjetController",
            'depenses'=>"API\DepenseController",
            'flux_finances'=>"API\FluxFinanceController",
            'entreprises'=>"API\EntrepriseController",
            'enterprises'=>"API\EntrepriseController",
            'chauffeurs'=>"API\ChauffeurController",

            'dashboard'=>"API\DashboardController",
            'avances'=>"API\AvanceController",
            'modeles'=>"API\ModeleController",
            'pays'=>"API\PaysController",
            'marques'=>"API\MarqueController",
            'options'=>"API\OptionController",
            'projets'=>"API\ProjetController" 
        ]
    );
});
