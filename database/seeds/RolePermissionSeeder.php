<?php

use App\Permission;
use Illuminate\Database\Seeder;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tables = ["entreprise","projet","permission","role",'utilisateur'];

        $permissions = [
               [
                   "fr"=>"ajoute",
                   "en"=>"create"
               ],
              [
                "fr"=>"supprime",
                "en"=>"delete"
            ],
            [
                "fr"=>"modifie",
                "en"=>"edit"
            ],
            [
                "fr"=>"accede",
                "en"=>"access"
            ],
            [
                "fr"=>"exporte",
                "en"=>"export"
            ]
        ];

    foreach($tables as $table)
    {
        foreach($permissions as $permission)
        {
            factory("App\Permission")->create([
                'nom' =>$permission['fr']."-".$table,
                'table' =>$table
                    ]);
                    
        }
    }

     $super = factory("App\Role")->create(['nom'=>"super-admin","libelle"=>"super administrateur"]);
     factory("App\Role")->create(['nom'=>"admin","libelle"=>"administrateur"]);
     factory("App\Role")->create(['nom'=>"gerant","libelle"=>"gérant"]);

     $permissions = Permission::all()->pluck('id');
     $super->permissions()->attach($permissions);

     $jos=factory('App\User')->create(['email'=>"dev@fontecsys.com","name"=>"Dev Pool","role_id"=>1]);
     $nene=factory('App\User')->create(['email'=>"associes@fontecsys.com","name"=>"Associés","role_id"=>1]);
     $tch=factory('App\User')->create(['email'=>"manager@fontecsys.com","name"=>"Manager","role_id"=>1]);
     $grt=factory('App\User')->create(['email'=>"admin@prepa.com", "name"=>"Admin", "username"=>"prepa", "role_id"=>3]);
  }
}
