<?php

use App\Membre;
use App\Entreprise;
use Illuminate\Database\Seeder;

class ProjetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;

        for($i;$i<3;$i++)
        {
            
            $entreprise = Entreprise::inRandomOrder()->take(2)->pluck('id');
            $membre = Membre::inRandomOrder()->take(2)->pluck('id');
            $secteur = SecteurActivite::inRandomOrder()->take(2)->pluck('id');

            $projet = factory('App\Projet')->create();
            $projet->entreprise()->attach($entreprise);
            $projet->entreprise()->attach($membre);
            $projet->entreprise()->attach($secteur);

            $projet->save();

        }
    }
}
