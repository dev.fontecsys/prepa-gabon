<?php

use Illuminate\Database\Seeder;
use App\Member;
use App\Pays;
use App\Competence;

class MembreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;

        for($i;$i<1;$i++)
        {
            
            $pays = Pays::inRandomOrder()->take(2)->pluck('id');
            $competences = Competence::inRandomOrder()->take(3)->pluck('id');

            $membre = factory('App\Membre')->create();
            $membre->competences()->attach($competences);
            $membre->nationalites()->attach($pays);

            $membre->save();

        }
    }
}
