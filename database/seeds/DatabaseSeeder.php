<?php

use App\Marque;
use App\Modele;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;
use App\Pays;
use App\TypeMotorisation;
use App\TypeVehicule;
use App\Vehicule;
use Illuminate\Support\Facades\Config;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $countries = Config::get('pays');
        foreach($countries as $country)
        {
            $a = ["code1"=>$country[1],"code2"=>$country[2],"libelle"=>$country[3]];
            factory('App\Pays')->create($a);
        }

        $secteurs = Config::get('secteurs');
        foreach($secteurs as $secteur)
        {
            factory('App\SecteurActivite')->create(["libelle"=>$secteur]);
        }



        $this->call(RolePermissionSeeder::class);
        $this->call(CompetenceSeeder::class);
        $this->call(FormationSeeder::class);
        $this->call(MembreSeeder::class);
        $this->call(EntrepriseSeeder::class);

       factory('App\Projet')->create();









    }
}
