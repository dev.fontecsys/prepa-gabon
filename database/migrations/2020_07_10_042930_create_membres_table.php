<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('membres', function (Blueprint $table) {


            $table->bigIncrements('id')->index();

            //Etat_civil
            $table->string('nom',50)->nullable();
            $table->string('prenom',50)->nullable();
            $table->date('date_naissance')->nullable();
            $table->string('promotion',4)->nullable();
            $table->text('description')->nullable();
            $table->string('avatar')->nullable();
            $table->enum('statut_marital',["célibataire","marié(e)","veuf(ve)","concubinage"])->default("célibataire");
            $table->enum('civilite',['dr','m.','mlle','mme','pr'])->default('m.');


            $table->string('cv')->nullable();
            //contact
            $table->string('phone1');
            $table->string('phone2')->nullable();
            $table->string('email');
            $table->string('siteweb')->nullable();
            $table->string('facebook')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('twitter')->nullable();

            //activite pro

            $table->unsignedBigInteger('pays_residence_id')->nullable();            
            $table->unsignedBigInteger('secteur_activite_id')->nullable();
            $table->unsignedBigInteger('formation_id')->nullable();
            $table->string('derniere_profession')->nullable();
            $table->string('activite_actuelle')->nullable();

            $table->unsignedBigInteger('created_by')->nullable();
            $table->timestamps();

            $table->foreign('secteur_activite_id')->references('id')->on('secteur_activites')->onDelete('set null');
            $table->foreign('formation_id')->references('id')->on('formations')->onDelete('set null');
            $table->foreign('pays_residence_id')->references('id')->on('pays')->onDelete('set null');
            $table->foreign('created_by')->references('id')->on('users')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('membres');
    }
}
