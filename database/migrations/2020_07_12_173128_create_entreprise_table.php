<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntrepriseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entreprises', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('nom_commercial');
            $table->string('logo')->nullable();
            $table->enum('forme_juridique',["EI","SUARL/EURL","SARL","SAS","SASU","SA","SNC","SCS","GIE","Autre","SP","SCI"])->default("SARL");
            $table->unsignedBigInteger('secteur_activite_id')->nullable();
            $table->string('secteur_activite_principal')->nullable();
            $table->text('autre_activite')->nullable();
            $table->enum('effectif',['1-5','6-10','11-15','16-20','+40'])->default("1-5");
           
            $table->string('phone1')->nullable();
            $table->string('phone2')->nullable();
            $table->string('email')->nullable();
            $table->string('siteweb')->nullable();
            $table->string('facebook')->nullable();
            $table->string('linkedin')->nullable();
            $table->string('twitter')->nullable();

            $table->string('rue')->nullable();
            $table->string('nr')->nullable();
            $table->string('bp')->nullable();
            $table->string('quartier')->nullable();
            $table->string('ville')->nullable();
            $table->unsignedBigInteger('pays_id')->nullable();
            $table->unsignedBigInteger('responsable_id')->nullable();
            $table->morphs('creatable'); 

            $table->timestamps();
            $table->foreign('responsable_id')->references('id')->on('membres')->onDelete('set null');
            $table->foreign('secteur_activite_id')->references('id')->on('secteur_activites')->onDelete('set null');
            $table->foreign('pays_id')->references('id')->on('pays')->onDelete('set null');

            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entreprises');

    }
}
