<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'description' => $faker->text,
        'avatar' => $faker->word,
        'role_id' => factory(App\Role::class),
        'username' => $faker->userName,
        'email_verified_at' => $faker->dateTime(),
        'password' => bcrypt("password"),
        'api_token' => Str::random(80),
        'remember_token' => Str::random(10),
        'last_sign_out_at' => $faker->dateTime(),
        'last_sign_in_at' => $faker->dateTime(),
    ];
});
