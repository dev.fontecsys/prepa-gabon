<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Pays;
use App\Membre;
use App\Entreprise;
use App\SecteurActivite;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Relations\Relation;

    $factory->define(App\Projet::class, function (Faker $faker) {
    $entreprise = Entreprise::inRandomOrder()->first();
    $pays = Pays::inRandomOrder()->first();
    $secteur = SecteurActivite::inRandomOrder()->first();
    $responsable = Membre::inRandomOrder()->first();
    $creatable = $faker->randomElement([
        App\User::class
    ]);

    return [
        'titre' => $faker->word,
        'responsable_id' => $responsable ? $responsable->id : factory(App\Membre::class),
        'secteur_activite_id' => $secteur ? $secteur->id : factory(App\SecteurActivite::class),
        'entreprise_id' => $entreprise ? $entreprise->id : factory(App\Entreprise::class),
        'pays_id' => $pays ? $pays->id : factory(App\Pays::class),
        'description' => $faker->text,
        'budget' => $faker->numberBetween($min = 100000, $max = 2000000),
        'date_debut_prev_prev' => $faker->date,
        'date_fin_prev' => $faker->date,
        'avancement' => $faker->numberBetween($min = 0, $max = 100),
        'visible' => $faker->boolean,
        'statut' => $faker->randomElement(['fermé', 'ouvert']),
        'creatable_id' => NULL ? $creatable->id : factory(App\User::class),
        'creatable_type' => array_search($creatable, Relation::$morphMap),
    ];
});
