<?php
  return 
   [
     ["titre"=>"Déploiement d'un serveur", "date_debut_prev_prev"=>"2020-03-10", "date_fin_prev"=>"2020-04-17", "description"=>"Le service de déploiement porte le nom de “Windows Deployment Services”. Il intègre toutes les fonctionnalités qui vont vous permettre de démarrer un poste client par le réseau, et de lui envoyer un système d’exploitation personnalisé."],
     ["titre"=>"Installation d'un reseau", "date_debut_prev_prev"=>"2020-03-10", "date_fin_prev"=>"2020-04-17", "description"=>"Installer un réseau Wi-Fi ou améliorer votre câblage réseau (informatique ou téléphonique) ? Trouvez une société de câblage ou un prestataire qui répond à vos besoins pour l'installation de votre réseau !"],
     ["titre"=>"Reparation d'un ascensseur", "date_debut_prev_prev"=>"2020-03-10", "date_fin_prev"=>"2020-04-17", "description"=>"RAS"],
     ["titre"=>"Formation", "date_debut_prev_prev"=>"2020-03-10", "date_fin_prev"=>"2020-04-17", "description"=>"RAS"],
   ] 
?>