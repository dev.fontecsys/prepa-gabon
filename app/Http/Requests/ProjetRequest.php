<?php

namespace App\Http\Requests;
use Illuminate\Foundation\Http\FormRequest;

class ProjetRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        return [
            'titre' =>"required",
            'avancement' =>"numeric|min:0|max:100",
            'budget' =>"numeric|min:0",
            'date_debut_prev' =>"required|date|before_or_equal:date_fin_prev",
            'date_fin_prev' =>"required|date|after_or_equal:date_debut_prev",
            'responsable_id' => 'required',
            'entreprise_id' => 'required'
        ];
    }


    public function messages()
    {
        return[
            "date_debut_prev.required"=>"La date de début est requise",
            "date_debut_prev.date"=>"Le format de la date est incorrect",
            "date_fin_prev.after_or_equal"=>"La date de début ne doit pas dépasser celle de fin",
            "date_fin_prev.required"=>"La date de fin est requise",
            "date_fin_prev.date"=>"Le format de la date est incorrect",
            "date_fin_prev.after_or_equal"=>"La date de fin doit dépasser celle de début",
            'titre.required' =>"Le nom du projet est requis",
            'budget.numeric' =>"Le budget doit être un nombre",
            'budget.min' =>"Le budget doit être un nombre positif",
            'avancement.numeric' =>"L'évolution du projet doit être un nombre",
            'avancement.min' =>"L'évolution du projet doit être un nombre positif",
            'avancement.max' =>"L'évolution du projet ne doit pas depasser 100 %",
            'responsable_id.required' => 'Vous devez choisir un responsable',
            'entreprise_id.required' => 'Vous devez choisir une entreprise', 
        ];
    }
}
