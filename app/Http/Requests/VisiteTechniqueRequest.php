<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VisiteTechniqueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            "nr"=>'required' ,
            'effectue_par' =>'required' ,
            'date_dbt' =>'required', 
            'vehicule_id' =>"required|exists:vehicules,id" ,

            'date_fin_prev' =>'required|after_or_equal:date_dbt', 
            'fichier' =>'required' ,
        ];
    }

    public function messages()
    {
        return [
            'vehicule_id.required' =>"Le véhicule à assurer est requis",
            'assureur_id.exists' =>"Cet assureur est inconnu",
            "nr.required"=>"Le numéro de la visite technique est requis",
            'effectue_par.required' =>"Le nom de l'autorité est requis",
            'date_dbt.required' =>"La date de début est requise",
            'date_fin_prev.required' =>"La date de fin est requise",
            'date_dbt.date' =>"La date de début n'est pas valide",
            'date_fin_prev.date' =>"La date de fin n'est pas valide",
            'date_fin_prev.after_or_equal' =>"La date doit être après la date de début",
            'fichier.required' =>"Veuillez charger une copie de la visite technique"
        ];
    }

}
