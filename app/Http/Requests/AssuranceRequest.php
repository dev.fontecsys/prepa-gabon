<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssuranceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nr"=>"required",
            'assureur_id' =>"required|exists:assureurs,id" ,
            'vehicule_id' =>"required|exists:vehicules,id" ,
            'date_dbt' =>"required|date",
            'date_fin_prev' =>"required|date|after_or_equal:date_dbt",
            'fichier' =>"required",
        ];
    }


    public function messages()
    {
        return
        [
            "nr.required"=>"Le numéro d'assurance est requis",
            'assureur_id.required' =>"Le nom de l'assureur est requis",
            'vehicule_id.required' =>"Le véhicule à assurer est requis",
            'assureur_id.exists' =>"Cet assureur est inconnu",
            'vehicule_id.exists' =>"Ce véhicule est inconnu",
            'date_dbt.required' =>"La date de début est requise",
            'date_fin_prev.required' =>"La date de fin est requise",
            'date_dbt.date' =>"La date de début n'est pas valide",
            'date_fin_prev.date' =>"La date de fin n'est pas valide",
            'date_fin_prev.after_or_equal' =>"La date de fin ne doit pas être avant la date de début",
            'fichier.required' =>"Veuillez charger une copie de l'assurance",
            ];
    }
}
