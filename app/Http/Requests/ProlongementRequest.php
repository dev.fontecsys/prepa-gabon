<?php

namespace App\Http\Requests;

use App\Rules\LocationDisponible;
use App\StatutPayement;
use Illuminate\Foundation\Http\FormRequest;

class ProlongementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_dbt' =>["required","date",($this->input('date_fin_prev')!="" && $this->input('vehicule_id') !="") 
            ?  new LocationDisponible($this->input('date_dbt'),$this->input('date_fin_prev'),$this->input('vehicule_id'),$this->method()=="PATCH" ? $this->input('id') : null) :''],
            'date_fin_prev' =>['required', 'date',"after_or_equal:date_dbt",
                        function ($attribute, $value, $fail) 
                        {
                            $time1 = explode(" ",$value);
                            $time2 = explode(" ",$this->input('date_dbt'));
                            if ($time1[1] != $time2[1]) 
                            {
                                $fail('Les dates de la location doivent avoir la même heure.');
                            }
                        }
            ],
            'location_id' => 'required|exists:locations,id',
            'vehicule_id' => 'required|exists:vehicules,id',
            'client_id' => 'required|exists:clients,id',
            'tarif' =>"required|numeric",
            'statut_payement_id' =>["required","exists:statut_payements,id",
            function ($attribute, $value, $fail) 
               {
                   $only = collect(['payé',"reservé","avance"]);
                   $statut= StatutPayement::whereId($value)->first();

                   if (!$only->contains($statut->nature)) 
                   {
                       $fail("Ce statut de payement n'est pas accepté");
                   }
               }
           ],
           'avance' =>$this->input('statut_payement_id')==2 ? "required|numeric|min:0" : "",


        ];
    }

    public function messages()
    {
      return  [
            "date_dbt.required"=>"La date de début est requise",
            "date_dbt.date"=>"Le format de la date est incorrect",
            "date_fin_prev.required"=>"La date de fin est requise",
            'tarif.required' =>"Le tarif est requis",
            'tarif.numeric' =>"Le tarif doit être un nombre",
            "date_fin_prev.date"=>"Le format de la date est incorrect",
            "date_fin_prev.after_or_equal"=>"La date de fin doit dépasser celle de début",
            'vehicule_id.required' =>"Le vehicule est requis",
            'vehicule_id.exists' =>"Ce vehicule est inconnu",
            'client_id.required' =>"Le client est requis",
            'client_id.exists' =>"Ce client est inconnu",
            'location_id.required' =>"L'identifiant de la location est requis",
            'location_id.exists' =>"Cette location  est inconnue",
            'statut_payement_id.required' =>"Le statut du payement est requis",
            'statut_payement_id.exists' =>"Ce statut de payement  est inconnu",
            'statut_payement_id.in' =>"Ce statut de payement n'est pas accepté",

            'avance.required' =>"L'avance est requise",
            'avance.numeric' =>"L'avance doit être un nombre",
            'avance.min' =>"L'avance doit être supérieur à 0",
      ];
    }
}
