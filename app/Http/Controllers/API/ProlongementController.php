<?php

namespace App\Http\Controllers\API;

use App\Events\CashAdvancedHasBeenMadeEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProlongementRequest;
use App\Location;
use Illuminate\Http\Request;

class ProlongementController extends Controller
{


    public function store(ProlongementRequest $request)
    {

        $location=Location::whereId($request->input('location_id'))->first();
        if($location)
        {

            $location=Location::create([
                'date_dbt'    => $request->input('date_dbt'),
                'date_fin_prev'    => $request->input('date_fin_prev'),
                'montant'     => $request->input('montant'),
                'nbr_jour'    => $request->input('nbr_jour'),
                "observation" => $request->input('observation'),
                'tarif' => $request->input('tarif'),
                'client_id'     => $location->client_id,
                'vehicule_id'     => $location->vehicule_id,
                'frais_livraison' => false,
                'lavage_carburant' => 0,
                "avec_chauffeur" => $location->avec_chauffeur,
                "client_est_chauffeur" => $location->client_est_chauffeur,
                "livraison_effective" => $location->livraison_effective,
                'statut_payement_id'=> $request->input("statut_payement_id"),
                "longue_duree" => $location->longue_duree,
                "niveau_carburant" => $location->niveau_carburant,
                "nom_chauffeur" => $location->nom_chauffeur,
                "parent_id" => $location->id,
                "nom_chauffeur_2" => $location->nom_chauffeur_2,
                "zone_location" => $location->zone_location

                ]);
                if($request->input("avance")!=null && $request->input("avance")!="")
                {
                 event(new CashAdvancedHasBeenMadeEvent($location, $request->input("avance")));  
                }
 
            return response()->json(['success' => true,'entity'=>$location->load(['client','vehicule'])],200);

        }
        else
        {
            return response()->json(['success' => false],200);
        }
    }
}
