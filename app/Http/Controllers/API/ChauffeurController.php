<?php

namespace App\Http\Controllers\API;

use App\Chauffeur;
use App\Exports\ChauffeurExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChauffeurRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Excel;

class ChauffeurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');

        return  Chauffeur::search($q)->orderBy("created_at",'desc')->paginate($per);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ChauffeurRequest $request)
    {
        try
        {
            DB::beginTransaction();
            $chauffeur = Chauffeur::create(
                [
                    'nom' =>$request->input('nom'),
                    'prenom' =>$request->input('prenom'),
                    'adresse' =>$request->input('adresse'),
                    'email' =>$request->input('email'),
                    'telephone1' =>$request->input('telephone1'),
                    'type_permis_conduire' =>$request->input('type_permis_conduire'),
                    'nr_permis_conduire' =>$request->input('nr_permis_conduire'),
                ]
                );
            
            //on upload le permis

            $file = $request->input('permis_conduire');
            $chauffeur->upload($file,$chauffeur->getMime($file),$chauffeur->id);
            

            DB::commit();
            return response()->json(['success' => true],201);

        }catch(\Exception $e)
        {
            DB::rollback();
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Chauffeur  $chauffeur
     * @return \Illuminate\Http\Response
     */
    public function show(Chauffeur $chauffeur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Chauffeur  $chauffeur
     * @return \Illuminate\Http\Response
     */
    public function edit(Chauffeur $chauffeur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Chauffeur  $chauffeur
     * @return \Illuminate\Http\Response
     */
    public function update(ChauffeurRequest $request, Chauffeur $chauffeur)
    {
        try
        {
            DB::beginTransaction();

            $chauffeur->nom =$request->input('nom');
            $chauffeur->prenom = $request->input('adresse');
            $chauffeur->telephone1 = $request->input('telephone1');
            $chauffeur->email = $request->input('email');
            $chauffeur->type_permis_conduire = $request->input('type_permis_conduire');
            $chauffeur->nr_permis_conduire = $request->input('nr_permis_conduire');
            $chauffeur->save();

            if($request->input('permis_conduire')!=$chauffeur->permis_conduire)
            {
                Log::info($request->input('permis_conduire'));
                //on upload le permis
                $file = $request->input('permis_conduire');
                $chauffeur->upload($file,$chauffeur->getMime($file),$chauffeur->id,$chauffeur->permis_conduire=="default_permis.jpg" ? "" : $chauffeur->permis_conduire);
            }
            
           
            DB::commit();
            return response()->json(['success' => true],200);

        }catch(\Exception $e)
        {
            DB::rollback();
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Chauffeur  $chauffeur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Chauffeur $chauffeur)
    {
        $chauffeur->delete();
        return response()->json(['success' => true],200);

    }

    public function getFile($id)
    {
        $chauffeur = Chauffeur::whereId($id)->first();
        $path = storage_path("/app/public/documents/permis/".$chauffeur->permis_conduire);


        if (file_exists($path)) 
        {
            ob_end_clean();

            return response()->download($path);
        }
        //return response()->download($path);
    }

    
    public function export() 
    {
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $format = request()->query('format') == null ? null : request()->query('format');
        $filename = "chauffeurs-".Carbon::now()->toDateTimeString();
        if($format)
        {
           if($format=="excel")
           return (new ChauffeurExport($q))->download($filename.'.xlsx');
           if($format=="pdf")
           return Excel::download(new ChauffeurExport($q),$filename.'.pdf');
        }
        
    }
}
