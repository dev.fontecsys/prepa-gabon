<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Location;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Http\Request;

class RapportFinanceController extends Controller
{
    public function par_annee(Request $request)
    {
        //cherche toutes locations de l'année
        $amount = 0;
        $jours = [];
        $locations = Location::where('vehicule_id',request()->input('vehicule_id'))->whereRaw("EXTRACT(YEAR from date_dbt) = ?", [request()->input('annee')])->get();
        foreach($locations as $location)
        {
            $dbt = Carbon::parse($location->date_dbt);
            $fin =Carbon::parse($location->date_fin_prev);
          
            /*if($dbt->month != $fin->month)
            {
                $period = CarbonPeriod::create(Carbon::parse($location->date_dbt)->toDateString(),Carbon::parse($location->date_fin_prev)->toDateString());
            
            
                // Iterate over the period
                foreach ($period as $date) 
                {
                    $amount += $location->tarif ;
                    array_push($jours,$date);
                }
            }
            else
            {
                
            }*/

            $amount += $location->montant ;

        }
        $mois = [];
        for($m=1; $m<=12; ++$m){
            array_push($mois,date('F', mktime(0, 0, 0, $m, 1)));
        }
        return [
            "annee" =>request()->input('annee'),
            "vehicule_id" =>request()->input('vehicule_id'),
            "total" =>$amount,
            "depenses" =>0,
            "jours" =>$jours,
            "mois" =>$mois
        ];



    }
}
