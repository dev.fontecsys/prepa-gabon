<?php

namespace App\Http\Controllers\API;

use App\Exports\FluxFinancesExport;
use App\FluxFinance;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use DB;
use Excel;

class FluxFinanceController extends Controller
{

    public function __construct()
    {
        DB::connection()->enableQueryLog();
    }
    public function index()
    {

        $incomings = 0;
        $outgoings = 0;
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $d_fin = request()->query('filter_date_fin_prev') == null ? null : request()->query('filter_date_fin_prev');
        $d_dbt = request()->query('filter_date_dbt') == null ? null : request()->query('filter_date_dbt');
        $annee= request()->query('annee') == null ? null : request()->query('annee');
        $mois = request()->query('mois') == null ? null : request()->query('mois');
        $vehicule = request()->query('vehicule_id') == null ? null : request()->query('vehicule_id');
        $type = request()->query('type') == null ? null : request()->query('type');

        $flux =  FluxFinance::with(['vehicule','financiable'])
                 ->search($q)->periode($d_dbt,$d_fin)
                 ->orderBy("flux_finances.created_at",'desc')->get();

        if($mois!=null && $annee!=null)
        {


           $flux =  FluxFinance::with(['vehicule'])
           ->searchPerMonthYear($mois,$annee)->search($q)
           ->vehiculeFilter($vehicule)
           ->typeFluxFilter($type)
           ->orderBy("flux_finances.created_at",'desc')->get();
           foreach($flux as $movement)
           {
               if( $movement->financiable instanceof \App\Location)
               {
                $incomings +=  $movement->montant;

               }
               else
               {
                $outgoings +=  $movement->montant;
               }
           }

        }        
        
        return response()->json(["flux"=> FluxFinance::with(['vehicule'])
        ->searchPerMonthYear($mois,$annee)->search($q)
        ->vehiculeFilter($vehicule)
        ->typeFluxFilter($type)
        ->orderBy("flux_finances.created_at",'desc')->paginate($per)
         ,"outgoings"=>$outgoings,"incomings"=>$incomings],200);
        
    }

    private function getFinanceOverTime($dbt,$fin)
    {
        
    }

    public function export() 
    {
        $q = request()->query('filter') == null ? null : request()->query('filter');
        $d_fin = request()->query('filter_date_fin_prev') == null ? null : request()->query('filter_date_fin_prev');
        $d_dbt = request()->query('filter_date_dbt') == null ? null : request()->query('filter_date_dbt');
        $annee= request()->query('annee') == null ? null : request()->query('annee');
        $mois = request()->query('mois') == null ? null : request()->query('mois');
        $type = request()->query('type') == null ? null : request()->query('type');

        $vehicule = request()->query('vehicule_id') == null ? null : request()->query('vehicule_id');
        return (new  FluxFinancesExport($q,$mois,$annee,$vehicule,$type))->download('flux-finances.xlsx');
    }
}
