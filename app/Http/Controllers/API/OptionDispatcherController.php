<?php

namespace App\Http\Controllers\Api;

use App\Pays;
use App\Role;
use App\User;
use App\Client;
use App\Marque;
use App\Option;
use App\Projet;
use App\Assureur;
use App\Vehicule;
use App\Formation;
use App\Competence;
use App\Permission;
use App\TypeOption;
use App\TypePermis;
use App\TypeDepense;
use App\TypeVehicule;
use App\StatutPayement;
use App\SecteurActivite;
use App\TypeMotorisation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OptionDispatcherController extends Controller
{

    public function get(Request $request,  $page)
    {
        //options pour la page vehicule
        if($page =="all")
        {
            return
            [
                "marques"=>[],
                "pays"=>Pays::all(),
                "secteurs"=>SecteurActivite::all(),
                "formations"=>Formation::all(),
                "competences"=>Competence::all(),
                "projets"=>Projet::all(),
                "tpermis"=>[],
                "voptions"=>[], 
            ];
        }
        if($page =="entreprises")
        {
            return
            [
                "marques"=>[],
                "pays"=>Pays::all(),
                "secteurs"=>SecteurActivite::all(),
                "motorisations"=>[],
                "tvehicules"=>[],
                "tpermis"=>[],
                "voptions"=>[],
            ];
        }
        //options pour la page vehicule
        if($page =="membres")
        {
            return
            [
                "marques"=>[],
                "pays"=>Pays::all(),
                "secteurs"=>SecteurActivite::all(),
                "formations"=>Formation::all(),
                "competences"=>Competence::all(),
                "tpermis"=>[],
                "voptions"=>[],
            ];
        }

        if($page =="projets")
        {
            return
            [
                "entreprise"=>Entreprise::all(),
                "projets"=>Projet::all(),
                "membres"=>Membre::all(),
            ];
        }

        if($page =="vehicule-details")
        {
            return
            [
                "assureurs"=>[],
            ];
        }
        if($page =="utilisateurs")
        {
            return
            [
                "roles"=>[],
            ];
        }
        if($page =="historiques")
        {
            return
            [
                "utilisateurs"=>[],
            ];
        }
        if($page =="pays")
        {
            return
            [
                "pays"=>Pays::all(),
            ];
        }
        if($page =="role-show")
        {
            return
            [
                "permissions"=>[],
            ];
        }
        if($page =="locations")
        {
            return
            [
                "vehicules"=>[],
                "clients"=>[],
                "chauffeurs"=>[],
                "payements"=>[]
            ];
        }
        if($page =="depenses")
        {
            return
            [
                "vehicules"=>[],
                "tdepenses"=>[]
            ];
        }
        if($page =="flux-finances")
        {
            return
                [
                    "vehicules"=>[]
                ];
        }
        if($page =="options")
        {
            return
            [
                "toptions"=>[]
            ];
        }
    }

}
