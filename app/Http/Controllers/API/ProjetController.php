<?php

namespace App\Http\Controllers\API;

use App\Entreprise;
use App\Projet;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetRequest;
use Illuminate\Support\Facades\Auth;

class ProjetController extends Controller 
{

    public function __construct()
    {
        $this->middleware("auth");
    }


    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 8 ;


        return   Projet::with(['entreprise'])->orderBy("created_at",'desc')->paginate($per);
    }
  
    public function create()
    {
        //
    }

    public function getAsParams()
    {
        $q = request()->query('query') == null ? null : request()->query('query');
        
        return  $q ?  Entreprise::select('id','entreprises.nom_commercial')->searchAsParameter($q)->paginate(8) : null ;
    }

    public function store(ProjetRequest $request)
    {
        try
        {
            DB::beginTransaction(); 
$request = $request->validated();
            if($request->input("description") != NULL){
                $description = $request->input("description");
            }else{
                $description = "Aucune description pour ce projet";
            }


            $projet = new Projet;

            $projet->titre = $request->input('titre');
            $projet->budget = $request->input("budget");
            $projet->date_debut_prev = $request->input("date_debut_prev");
            $projet->date_fin_prev = $request->input("date_fin_prev");
            $projet->avancement = $request->input('avancement');
            $projet->responsable_id = $request->input("responsable_id");
            $projet->entreprise_id = $request->input('entreprise_id');
            $projet->description =  $description;
            $projet->creatable_type = "App\User";
            $projet->creatable_id= Auth::user()->id;

            $projet->save();
            
    
            DB::commit();
            return response()->json(['success' => true,'projet'=> $projet]);

        }
        catch(\Exception $e)
        {
            DB::rollback();
            return ['status'=>false,'message'=>$e->getMessage()];  
        }
    }

    public function show()
    {

    }

    public function edit()
    {
        
    }

    public function update(ProjetRequest $request, Projet $projet)
    {
        try
        {
            DB::beginTransaction(); 

            // $request = $request->validated();

            if($request->input("description") != NULL){
                $description = $request->input("description");
            }else{
                $description = "Aucune description pour ce projet";
            }

            $projet->titre = $request->input('titre');
            $projet->budget = $request->input("budget");
            $projet->date_debut_prev = $request->input("date_debut_prev");
            $projet->date_fin_prev = $request->input("date_fin_prev");
            $projet->avancement = $request->input('avancement');
            $projet->responsable_id = $request->input("responsable_id");
            $projet->entreprise_id = $request->input('entreprise_id');
            $projet->description = $description;
            $projet->creatable_type = "App\User";
            $projet->creatable_id= Auth::user()->id;
                
            $projet->save();
            
            DB::commit();
            return response()->json(['success' => true,'projet'=> $projet]);
        }
        catch(\Exception $e)
        {
            DB::rollback();
            Log::info($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);  
        }
    }

    public function destroy(Projet $Projet)
    {
        //on supprime
        $Projet->delete();
        return response()->json(['message' => 'Projet supprimé avec succès'],200);
    }

    public function export() 
    {
        
    }
}
