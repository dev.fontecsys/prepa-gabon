<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\VisiteTechniqueRequest;
use App\VisiteTechnique;

use DB;
use Illuminate\Support\Facades\Log;

class VisiteTechniqueController extends Controller
{
        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VisiteTechniqueRequest $request)
    {
        
        try
        {

            //on commence la transaction
            DB::beginTransaction();

            $data = $request->toArray();

            unset($data['fichier']);

            $file = $request->input('fichier');

            $entity = VisiteTechnique::create($data);

             
            $entity->fresh()->load(['vehicule']);

            $entity->upload($file,"data:application/pdf;base64",$entity->vehicule->plaque_immatriculation);

            DB::commit();

            return response()->json(['success' => true,'entity'=>$entity->fresh()->unsetRelation('vehicule')],201);
 
        }
        catch(\Exception $e)
        {
                DB::rollback();
                return ['status'=>false,'message'=>$e->getMessage()];

        }

    }

    public function getFile($id)
    {
        $vt = VisiteTechnique::whereId($id)->first();
        $path = storage_path("/app/public/documents/visites_techniques/".$vt->fichier);


        if (file_exists($path)) {
            return response()->download($path);
        }
        //return response()->download($path);
    }
}
