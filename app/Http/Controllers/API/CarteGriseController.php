<?php

namespace App\Http\Controllers\API;

use App\CarteGrise;
use App\Http\Controllers\Controller;
use App\Http\Requests\CarteGriseRequest;
use App\Vehicule;
use Illuminate\Http\Request;
use DB;
use Illuminate\Http\Response;
use Log;

class CarteGriseController extends Controller
{
        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CarteGriseRequest $request)
    {
        try
        {

            //on commence la transaction
            DB::beginTransaction();

            $data = $request->toArray();

            unset($data['fichier']);

            $file = $request->input('fichier');
            $vehicule = Vehicule::whereId($request->input('vehicule_id'))->first();

            $entity = CarteGrise::create([

                'date_etablissement' =>$request->input('date_etablissement'),
                'vehicule_id' =>$vehicule->id,
                'proprietaire' =>$request->input('proprietaire'),
                'profession' =>$request->input('profession'),
                'adresse' =>$request->input('adresse'),
                'nr_immatriculation' =>$vehicule->plaque_immatriculation,
                "date_premiere_mise_en_circulation"=>$vehicule->date_mise_en_circulation,
                 "vehicule_neuf"=> false,
                 "poids_vide"=>$vehicule->poids,
                "nbr_place"=>$vehicule->nbr_place,    
                "carosserie"=>$vehicule->couleur,
                'marque_id'=>$vehicule->marque_id,
                'modele_id'=>$vehicule->modele_id,
                'type_motorisation_id'=>$vehicule->type_motorisation_id
            ]);

             
            $entity->fresh()->load(['vehicule']);

            $entity->upload($file,"data:application/pdf;base64",$entity->vehicule->plaque_immatriculation);

            DB::commit();

            return response()->json(['success' => true,'entity'=>$entity->fresh()->unsetRelation('vehicule')],201);
 
        }
        catch(\Exception $e)
        {
                DB::rollback();
                return ['status'=>false,'message'=>$e->getMessage()];

        }
    }


    public function getFile($id)
    {
        $carte = CarteGrise::whereId($id)->first();
        $path = storage_path("/app/public/documents/cartes_grises/".$carte->fichier);


        if (file_exists($path)) {
            return response()->download($path);
        }
        //return response()->download($path);
    }
}
