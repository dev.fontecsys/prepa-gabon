<?php

namespace App\Http\Controllers\API;

use App\Depense;
use App\Exports\DepenseExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\DepenseRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Excel;

class DepenseController extends Controller
{
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');
        return    Depense::with(['vehicule','type_depense'])->search($q)->orderBy("depenses.created_at",'desc')->paginate($per);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DepenseRequest $request)
    {
        //dd($request->input('date_facturation'));

        Depense::create($request->toArray());
        return response()->json(["succes"=>true],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Depense  $depense
     * @return \Illuminate\Http\Response
     */
    public function show(Depense $depense)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Depense  $depense
     * @return \Illuminate\Http\Response
     */
    public function edit(Depense $depense)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\DepenseRequest $request
     * @param  \App\Depense  $depense
     * @return \Illuminate\Http\Response
     */
    public function update(DepenseRequest $request, Depense $depense)
    {

        
        $depense->description = $request->input('description');
        $depense->vehicule_id = $request->input('vehicule_id');
        $depense->type_depense_id = $request->input('type_depense_id');
        $depense->montant = $request->input('montant');
        $depense->date_facturation = $request->input('date_facturation');
        $depense->save();

        return response()->json(["succes"=>true],201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Depense  $depense
     * @return \Illuminate\Http\Response
     */
    public function destroy(Depense $depense)
    {
        $depense->delete();
        return response()->json(['success' => true],200);
    }

    public function export() 
    {

        // $q = request()->query('filter') == null ? null : request()->query('filter');
        // $d_fin = request()->query('filter_date_fin_prev') == null ? null : request()->query('filter_date_fin_prev');
        // $d_dbt = request()->query('filter_date_dbt') == null ? null : request()->query('filter_date_dbt');
        // $annee= request()->query('annee') == null ? null : request()->query('annee');
        // $mois = request()->query('mois') == null ? null : request()->query('mois');
        // $vehicule = request()->query('vehicule_id') == null ? null : request()->query('vehicule_id');

        $q = request()->query('filter') == null ? null : request()->query('filter');
        $format = request()->query('format') == null ? null : request()->query('format');
        $filename = "depenses-".Carbon::now()->toDateTimeString();
        if($format)
        {
           if($format=="excel")
           return (new DepenseExport($q))->download($filename.'.xlsx');
           if($format=="pdf")
           return Excel::download(new DepenseExport($q),$filename.'.pdf');
        }
        
    }
}
