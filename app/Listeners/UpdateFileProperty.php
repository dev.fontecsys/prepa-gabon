<?php

namespace App\Listeners;

use App\Events\DocsOnVehicleCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class UpdateFileProperty
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DocsOnVehicleCreated  $event
     * @return void
     */
    public function handle(DocsOnVehicleCreated $event)
    {
        if($event->document instanceof \App\Assurance)
        {
            $event->document->fichier = $event->filename;
            $event->document->save();
        }
        if($event->document instanceof \App\CarteGrise)
        {
            $event->document->fichier = $event->filename;
            $event->document->save();
        }
        if($event->document instanceof \App\VisiteTechnique)
        {
             $event->document->fichier = $event->filename;
             $event->document->save();
        }
        if($event->document instanceof \App\Vehicule)
        {
             $event->document->image_mise_en_avant = $event->filename;
             $event->document->save();
        }
        if($event->document instanceof \App\Client)
        {
             $event->document->piece_identite = $event->filename;
             $event->document->save();
        }
        if($event->document instanceof \App\User)
        {
             $event->document->avatar = $event->filename;
             $event->document->save();
        }
        if($event->document instanceof \App\Chauffeur)
        {
             $event->document->permis_conduire = $event->filename;
             $event->document->save();
        }
        
    }
}
