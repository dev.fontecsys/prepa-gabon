<?php

namespace App\Listeners;

use App\Events\CashAdvancedHasBeenMadeEvent;
use App\FluxFinance;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class AddFluxFinanceItem
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CashAdvancedHasBeenMadeEvent  $event
     * @return void
     */
    public function handle(CashAdvancedHasBeenMadeEvent $event)
    {
        $location = $event->location;
        $jours = ($location->nbr_jour > 1) ? "jours":"jour";
        $prefix1 =  ($location->statutPayement->nature=="payé") ? "Paiement total" : (($location->statutPayement->nature=="avance") ? "Avance sur paiement" : "" );
        $prefix2 = $location->parent_id!=null ? " : Prolongement n°".$location->numero : ": Location n° ".$location->numero ;
        $description =$prefix1." ".$prefix2." pour ".$location->nbr_jour." ".$jours." : Du "
                      .Carbon::parse($location->date_dbt)->format('d-m-Y')." au "
                      .Carbon::parse($location->date_fin_prev)->format('d-m-Y');

        
           //on génére un flux financier
          
          FluxFinance::create(
            [
              "flux"=> $description,
              "montant"=> $event->montant,
              "financiable_id"=> $location->id,
              "vehicule_id"=> $location->vehicule_id,
              "date_transaction"=>Carbon::now()->format('Y-m-d'),
              "financiable_type"=>"App\Location",

            ]);
    }
}
