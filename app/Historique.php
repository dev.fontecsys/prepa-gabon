<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Historique extends Activity
{
    //

    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                    ->orWhere('activity_log.log_name', 'LIKE', "%{$q}%")
                    ->orWhere('activity_log.subject_type', 'LIKE', "%{$q}%")
                    ->orWhere('activity_log.description', 'LIKE', "%{$q}%")
                    ->orWhere('activity_log.created_at', 'LIKE', "%{$q}%")
                    ->orWhere('activity_log.properties', 'LIKE', "%{$q}%")
                    ->orWhere('users.name', 'LIKE', "%{$q}%")
                    ->leftJoin('users', 'users.id', '=', 'activity_log.causer_id');
    }

    public function scopeCausedBy(Builder $query, Model $causer = null): Builder
    {

        if($causer!=null)
        return $query
            ->where('causer_type', $causer->getMorphClass())
            ->where('causer_id', $causer->getKey());
        else 
        return $query;
    }

    public function scopeForSubject(Builder $query, Model $subject = null): Builder
    {
        if($subject!=null)
        return $query
            ->where('subject_type', $subject->getMorphClass())
            ->where('subject_id', $subject->getKey());
        else
        return $query;
    }
    public function scopePeriode($query,$d_dbt,$d_fin)
    {
        if($d_fin && $d_dbt) 
        {
            $d_dbt = Carbon::parse($d_dbt)->toDateTimeString();
            $d_fin= Carbon::parse($d_fin)->addDay()->subMinute()->toDateTimeString();
            return $query->whereBetween('activity_log.created_at',[$d_dbt,$d_fin]);
        } 
        else
            return $query;
    }
}
