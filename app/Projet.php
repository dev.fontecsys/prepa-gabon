<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projet extends Model
{
    protected $guarded=[];

            /**
     * Get the owning commentable model.
     */
    public function creatable()
    {
        return $this->morphTo();
    }

    public function entreprise()
    {
        return $this->belongsTo("App\Entreprise", "entreprise_id");
    }
    
    public function membre()
    {
        return $this->morphTo("App\Membre", "responsable_id");

    } 

    public function secteur()
    {
        return $this->belongsTo("App\SecteurActivite", "secteur_activite_id");
    } 

    public function pays()
    {
        return $this->belongsTo("App\Pays","pays_id");

    }
}
