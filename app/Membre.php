<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Membre extends Model
{
    protected $guarded=[];
    protected $appends =["hasPhoto",'fullName','fullNameWithTitel'];



    public function getHasPhotoAttribute()
    {
        if($this->avatar == null) return false;
        return file_exists(storage_path("/app/public/featured/".$this->avatar)) ? true : false;
    }
    public function getFullNameAttribute()
    {
        return ucfirst($this->prenom)." ".ucfirst($this->nom);
    }

    public function getFullNameWithTitelAttribute()
    {
        return ucfirst($this->civilite)." ".ucfirst($this->prenom)." ".ucfirst($this->nom);
    }

    public function scopeResidence($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->where('membres.pays_residence_id',$q);
    }

    public function scopeSecteur($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->where('membres.secteur_activite_id',$q);
    }
    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->orWhere('membres.nom', 'LIKE', "%{$q}%")
                ->orWhere('membres.prenom', 'LIKE', "%{$q}%")
                ->orWhere('membres.derniere_profession', 'LIKE', "%{$q}%")
                ->orWhere('membres.activite_actuelle', 'LIKE', "%{$q}%")
                ->orWhere('membres.phone1', 'LIKE', "%{$q}%")
                ->orWhere('membres.phone2', 'LIKE', "%{$q}%")
                ->orWhere('membres.email', 'LIKE', "%{$q}%")
                ->orWhere('membres.prenom', 'LIKE', "%{$q}%")
                ->orWhere('membres.statut_marital', 'LIKE', "%{$q}%");
                //->orWhere('roles.libelle', 'LIKE', "%{$q}%")
                //->join('roles', 'roles.id', '=', 'users.role_id');
    }

    public function scopeSearchAsParameter($query, $q)
    {
        if ($q == null) return $query;
        return $query->orWhere('membres.nom', 'LIKE', "%{$q}%")
                     ->orWhere('membres.prenom', 'LIKE', "%{$q}%");
    }

    public function scopePromotion($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->where('membres.promotion',$q);
    }

    public function nationalites()
    {
        return $this->belongsToMany("App\Pays", 'nationalite_membre', 'membre_id', 'nationalite_id');

    }

    public function competences()
    {
        return $this->belongsToMany("App\Competence", 'competence_membre', 'membre_id', 'competence_id');

    }

    public function residence()
    {
        return $this->belongsTo("App\Pays","pays_residence_id");

    }

    public function formation()
    {
        return $this->belongsTo("App\Formation");

    }
    public function secteur()
    {
        return $this->belongsTo("App\SecteurActivite","secteur_activite_id");

    }

    public function creator()
    {
        return $this->belongsTo("App\User","created_by");
 
    }

        /**
     * Get all of the member's enterprises.
     */
    public function entreprises()
    {
        return $this->morphMany('App\Entreprise', 'creatable');
    }

            /**
     * Get all of the member's enterprises.
     */
    public function projets()
    {
        return $this->morphMany('App\Projet', 'creatable');
    }

    

    
}
