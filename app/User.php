<?php

namespace App;

use App\Traits\CanUpload;
use App\Traits\HasRoles;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
        use Notifiable,HasRoles,CanUpload,LogsActivity,HasApiTokens;

    protected static $logAttributes = ["name","email","description",'role_id'];
    protected static $logName = 'utilisateur';
    protected static $logOnlyDirty = true;   
    protected static $submitEmptyLogs = false;
    

    protected $storage_path ="public/avatars";


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id','api_token',"username"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['hasAvatar',"permissions"];

    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé l'utilisateur <strong>{$this->name}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé l'utilisateur <strong>{$this->name}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié  l'utilisateur <strong>{$this->name}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié l'utilisateur <strong>{$this->name}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté  l'utilisateur <strong>{$this->name}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté  l'utilisateur <strong>{$this->name}</strong>";
        }
        
    }

    public function getHasAvatarAttribute()
    {
        if($this->avatar==null)
        return false;
        else
        {
            return file_exists(storage_path("/app/public/avatars/".$this->avatar));
        }
    }
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    public function getPermissionsAttribute()
    {
           return $this->permissions();
    }

    
    public function permissions()
    {
      if(!$this->hasAnyRole())
          return collect([]);
      else
        return $this->role->hasPermissions() ? $this->role->permissions->flatten()->pluck("nom") : collect([]);
    }

    public function hasAnyRole()
    {
      return $this->role!=null ? true : false ;
    }
    
    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->orWhere('users.email', 'LIKE', "%{$q}%")
                ->orWhere('users.name', 'LIKE', "%{$q}%")
                ->orWhere('users.created_at', 'LIKE', "%{$q}%");
                //->orWhere('roles.libelle', 'LIKE', "%{$q}%")
                //->join('roles', 'roles.id', '=', 'users.role_id');
    }
    public function scopeFilterRole($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->where('users.role_id', $q);
                //->orWhere('roles.libelle', 'LIKE', "%{$q}%")
                //->join('roles', 'roles.id', '=', 'users.role_id');
    }

            /**
     * Get all of the user's enterprises.
     */
    public function entreprises()
    {
        return $this->morphMany('App\Entreprise', 'creatable');
    }
}
