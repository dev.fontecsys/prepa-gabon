<?php

namespace App\Rules;

use App\Location;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class LocationDisponible implements Rule
{

    protected $date_dbt,$date_fin_prev,$vehicule_id,$location_id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($date_dbt,$date_fin_prev,$vehicule_id,$location_id=null)
    {
       $this->date_dbt = $date_dbt;
       $this->date_fin_prev = $date_fin_prev;
       $this->vehicule_id = $vehicule_id;
       $this->location_id = $location_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $location = Location::periodSearch($this->date_dbt,$this->date_fin_prev) 
                    ->filterbyVehicle($this->vehicule_id)
                    ->where(function($query) 
                    {
                       if($this->location_id==null) return $query;
                       else return $query->where('id','<>',$this->location_id);
                    })->first();
        $result  = $location ? false : true;

        if (App::environment('local')) 
        {
            // The environment is local
            Log::info(DB::getQueryLog());
        }
        
        
        //dd($location);
        return $result;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Cette voiture est occupée durant cette période';
    }
}
