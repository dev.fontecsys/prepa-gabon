<?php

namespace Tests\Feature;

use App\Location;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class ProlongerLocationTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function un_utilisateur_non_authentifie_ne_peut_pas_prolonger_une_location()
    {
        $this->withoutExceptionHandling();

        $this->expectException('Illuminate\Auth\AuthenticationException');

        $location = create('App\Location');

        $this->setDataThenPost($location);

    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function un_utilisateur_authentifie_peut_prolonger_une_location()
    {
        $this->signInAPI();


        $this->withoutExceptionHandling();
        $location = create('App\Location');

        $this->setDataThenPost($location)->assertStatus(200);
        $this->assertDatabaseHas("locations",['observation'=>"Ok mon premier test joslin"]);

    }

    /** @test */
    public function le_prolongement_d_une_location_a_besoin_de_validation()
    {
        $this->signInAPI();
        //on a une location
        $location = create('App\Location');
        //Le prolongement d'une location a besoin d'une location donnée
       $this->setDataThenPost($location,['key'=>'location_id','value'=>""])->assertStatus(422);
        //Le prolongement d'une location a besoin d'une location qui existe dans la base de donnée
        $this->setDataThenPost($location,['key'=>'location_id','value'=>"XDSD"])->assertStatus(422);
        //Le prolongement d'une location a besoin  d'une date de début
        $this->setDataThenPost($location,['key'=>'date_dbt','value'=>""])->assertStatus(422);
        //Le prolongement d'une location a besoin  d'une date de debut avec le format d'une date
        $this->setDataThenPost($location,['key'=>'date_dbt','value'=>"XXfFFF"])->assertStatus(422);
        //Le prolongement d'une location a besoin  d'une date de fin
        $this->setDataThenPost($location,['key'=>'date_fin_prev','value'=>""])->assertStatus(422);
        //Le prolongement d'une location a besoin  d'une date de fin avec le format d'une date
        $this->setDataThenPost($location,['key'=>'date_fin_prev','value'=>"XDSD"])->assertStatus(422);

        $this->setDataThenPost($location,[['key'=>"date_dbt","value"=>$location->date_fin_prev],['key'=>"date_fin_prev","value"=>Carbon::parse($location->date_fin_prev)->subDays(10)->toDateTimeString()]])
             ->assertStatus(422);

    }

    /**
     * @param $location
     * @return mixed
     */
    public function setDataThenPost($location,$fields = [])
    {
        //On recupere les données de l'ancienne location
        $data = $location->toArray();


        //On change les dates de dbt et de fin de la nouvelle location
        $d = Carbon::parse($location->date_fin_prev);
        $data['date_dbt'] = $d->toDateTimeString();
        $data['date_fin_prev']=$d->addDays(3)->toDateTimeString();

        $data['nbr_jour'] = 5;
        $data['location_id'] = $location->id;
        $data['frais_livraison'] = 0;
        $data['lavage_carburant'] = 0;
        $data['montant'] = $data['nbr_jour'] * doubleval($data['vehicule']['tarif']);
        $data['montant_ttc'] = $data['nbr_jour'] * doubleval($data['vehicule']['tarif']) * (118 / 100);
        $data['observation'] = "Ok mon premier test joslin";

        //Si Fields est un tableau a une dimension
        if (count($fields) == count($fields, COUNT_RECURSIVE) && count($fields, COUNT_RECURSIVE)==2 )
        {
            if(array_key_exists("key",$fields) && array_key_exists("value",$fields))
                $data[$fields["key"]] = $fields["value"];
        }
        //sinon il est à 2 dimensions
        else
        {
            foreach ($fields as $field)
            {
                if(is_array($field))
                    if(array_key_exists("key",$field) && array_key_exists("value",$field))
                        $data[$field["key"]] = $field["value"];
            }
        }

        return $this->json('post','api/locations/prolongement', $data);
    }


}
