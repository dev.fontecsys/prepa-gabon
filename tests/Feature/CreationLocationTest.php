<?php

namespace Tests\Feature;

use App\FluxFinance;
use App\Location;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class CreationLocationTest extends TestCase
{
    use RefreshDatabase;

    private $statut1,$statut2,$statut3;

    protected function setUp(): void
    {
        parent::setUp();
        $this->statut1  = create('App\StatutPayement',['nature'=>"payé"]);
        $this->statut2  = create('App\StatutPayement',['nature'=>"avance"]);
        $this->statut3  = create('App\StatutPayement',['nature'=>"reservé"]);
    }

    /** @test */
    function un_utilisateur_non_authentifie_ne_peut_pas_ajouter_une_location()
    {
        $this->withoutExceptionHandling();

        $this->expectException('Illuminate\Auth\AuthenticationException');

        $location = make('App\Location');

        return $this->json('post','api/locations', $location->toArray());

    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function un_utilisateur_authentifie_peut_ajouter_une_location()
    {
        $this->signInAPI();

      
        $this->withExceptionHandling();


        $vehicule = create('App\Vehicule');
        $location = make('App\Location',['vehicule_id'=>$vehicule->id,"statut_payement_id"=>$statut1->id],"")->toArray();
        $client = make('App\Client')->toArray();

     
        $location['client']= $client ;
        $location['ancien_client'] = true ;

        $response = $this->json('post','api/locations', $location);
        $response->assertStatus(201);
        $this->assertDatabaseHas("locations",['vehicule_id'=>$vehicule->id]);

    }

        /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function un_utilisateur_authentifie_peut_ajouter_une_location_en_payant_une_avance()
    {
        $this->signInAPI();

      
        $this->withExceptionHandling();

        $location = make('App\Location',['avance'=>5000,"statut_payement_id"=>$this->statut2->id],"")->toArray();
 
        $response = $this->json('post','api/locations', $location);
        $response->assertStatus(201);
        $this->assertDatabaseHas("flux_finances",['id'=>1,"montant"=>5000]);

    }


            /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function un_utilisateur_authentifie_peut_ajouter_une_location_reservee_mais_elle_ne_produit_aucun_flux_de_finance()
    {
        $this->signInAPI();

      
        $this->withExceptionHandling();


        $location = make('App\Location',["statut_payement_id"=>$this->statut3->id],"")->toArray();
 
        $response = $this->json('post','api/locations', $location);
        $response->assertStatus(201);
        $this->assertDatabaseMissing("flux_finances",['id'=>1]);

    }
      /**
     * A basic feature test example.
     *
     * @return void
     */
    /** @test */
    public function un_utilisateur_authentifie_peut_ajouter_un_flux_en_ajoutant_une_location()
    {
        $this->signInAPI();

        $flux = FluxFinance::all();

       


        $this->withExceptionHandling();
        $this->assertDatabaseMissing("flux_finances",['id'=>1]);

        $vehicule = create('App\Vehicule');
        $location = make('App\Location',['vehicule_id'=>$vehicule->id,'statut_payement_id'=>$this->statut1->id])->toArray();
        $client = make('App\Client')->toArray();


        $location['client']= $client ;
        $location['ancien_client'] = true ;
        $response = $this->json('post','api/locations', $location);
        $response->assertStatus(201);
        $this->assertDatabaseHas("flux_finances",['id'=>1]);

    }
    /** @test */
    public function un_utilisateur_authentifie_peut_supprimer_une_location()
    {
        $this->signInAPI();
        $this->withExceptionHandling();
        //given we have a location


        $location = create('App\Location',['statut_payement_id'=>$this->statut1->id]);
        //$location = create('App\Location',["date_dbt"=>'2020-02-12',"date_fin_prev"=>'2020-03-16',]);

        //on lance la requete de suppression
        $response = $this->json('delete','api/locations/'.$location->id);

        //on verifie que y a plus de location
        $this->assertDatabaseMissing("locations",['id'=>$location->id]);
        //Et que les flux ont disparu aussi
        $this->assertDatabaseMissing("flux_finances",['financiable_id'=>$location->id]);

    }
    /** @test */
    public function un_utilisateur_authentifie_peut_ajouter_une_location_avec_un_nouveau_client()
    {
        $this->signInAPI();



        $this->withExceptionHandling();
        $location = make('App\Location',['statut_payement_id'=>$this->statut3->id])->toArray();
        $client = make('App\Client')->toArray();


        $location['client']= $client ;
        $location['ancien_client'] = false ;
        $response = $this->json('post','api/locations', $location);
        $response ->assertStatus(201);
        $this->assertDatabaseHas("locations",['id'=>1]);

    }
    /** @test */
    public function la_creation_d_une_location_a_besoin_de_validation()
    {
        $this->signInAPI();
        //on a une location
        $location = create('App\Location',["tarif"=>2000]);
        //Le prolongement d'une location a besoin d'une location donnée
       $this->setDataThenPost($location,['key'=>'location_id','value'=>""])->assertStatus(422);
        //Le prolongement d'une location a besoin d'une location qui existe dans la base de donnée
        $this->setDataThenPost($location,['key'=>'location_id','value'=>"XDSD"])->assertStatus(422);
        //Le prolongement d'une location a besoin  d'une date de début
        $this->setDataThenPost($location,['key'=>'date_dbt','value'=>""])->assertStatus(422);
        //Le prolongement d'une location a besoin  d'une date de debut avec le format d'une date
        $this->setDataThenPost($location,['key'=>'date_dbt','value'=>"XXfFFF"])->assertStatus(422);
        //Le prolongement d'une location a besoin  d'une date de fin
        $this->setDataThenPost($location,['key'=>'date_fin_prev','value'=>""])->assertStatus(422);
        //Le prolongement d'une location a besoin  d'une date de fin avec le format d'une date
        $this->setDataThenPost($location,['key'=>'date_fin_prev','value'=>"XDSD"])->assertStatus(422);

        $this->setDataThenPost($location,[['key'=>"date_dbt","value"=>$location->date_fin_prev],['key'=>"date_fin_prev","value"=>Carbon::parse($location->date_fin_prev)->subDays(10)->toDateTimeString()]])
             ->assertStatus(422);

    }

    /**
     * @param $location
     * @return mixed
     */
    public function setDataThenPost($location,$fields = [])
    {
        //On recupere les données de l'ancienne location
        $data = $location->toArray();


        //On change les dates de dbt et de fin de la nouvelle location
        $d = Carbon::parse($location->date_fin_prev);
        $data['date_dbt'] = $d->toDateTimeString();
        $data['date_fin_prev']=  $d->addDays(3)->toDateTimeString();

        $data['nbr_jour'] = Carbon::parse($data['date_fin_prev'])->diffInDays(Carbon::parse($data['date_dbt']));
        $data['location_id'] = $location->id;
        $data['frais_livraison'] = 0;
        $data['lavage_carburant'] = 0;
        $data['montant'] = $data['nbr_jour'] * $data['tarif'];
        $data['observation'] = "Ok mon premier test joslin";

        //Si Fields est un tableau a une dimension
        if (count($fields) == count($fields, COUNT_RECURSIVE) && count($fields, COUNT_RECURSIVE)==2 )
        {
            if(array_key_exists("key",$fields) && array_key_exists("value",$fields))
                $data[$fields["key"]] = $fields["value"];
        }
        //sinon il est à 2 dimensions
        else
        {
            foreach ($fields as $field)
            {
                if(is_array($field))
                    if(array_key_exists("key",$field) && array_key_exists("value",$field))
                        $data[$field["key"]] = $field["value"];
            }
        }

        return $this->json('post','api/locations', $data);
    }


}
