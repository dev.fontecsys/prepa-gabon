<?php

namespace Tests\Unit;

use App\FluxFinance;
use App\RapportFinance;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;

use phpDocumentor\Reflection\Types\Void_;
use Tests\TestCase;

class LocationTest extends TestCase
{
    use RefreshDatabase;

    protected $location;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /** @test */
    function une_location_genere_un_ou_plusieurs_flux_financiers_apres_insertion()
    {
        $now = Carbon::parse('2020-02-01 08:00:00');
        
        //Une location sur le mm mois
        $now = Carbon::parse('2020-02-01');
        $loc =create('App\Location',['date_dbt'=>$now->toDateTimeString(),"date_fin_prev"=>$now->addDays(4)->toDateTimeString()]);
        $this->assertDatabaseHas('flux_finances',["financiable_id"=>$loc->id,'id'=>1]);
        //une location sur 2 mois #
        $now = Carbon::parse('2020-02-01');
        $loca =create('App\Location',['date_dbt'=>$now->toDateTimeString(),"date_fin_prev"=>$now->addMonth(1)->toDateTimeString()]);
        $flux =FluxFinance::where('financiable_id',$loca->id)->get();
        $this->assertCount(2,$flux);

        //une location sur 3 mois #
        $now = Carbon::parse('2020-02-01');
        $loco =create('App\Location',["tarif"=>20000,'date_dbt'=>$now->toDateTimeString(),"date_fin_prev"=>$now->addDays(5)->addMonths(4)->toDateTimeString()]);
        $loco->fresh();
        $fluxz =FluxFinance::where('financiable_id',$loco->id)->get();


    }

    /** @test */
    function la_somme_des_flux_dune_location_est_egale_au_montant_de_la_location()
    {

        //une location sur 3 mois #
        $now = Carbon::parse('2020-02-01');
        $loco =create('App\Location',["tarif"=>20000,'date_dbt'=>$now->toDateTimeString(),"date_fin_prev"=>$now->addDays(5)->addMonths(4)->toDateTimeString()]);
        $fluxz =FluxFinance::where('financiable_id',$loco->id)->get();
        $this->assertEquals($loco->montant,$fluxz->sum('montant'));
    }
    /** @test */
    function une_location_a_une_voiture()
    {
        $location = create("App\Location");
        $this->assertInstanceOf('App\Vehicule', $location->vehicule);
    }

    /** @test */
    function une_location_a_un_client()
    {
        $location = create("App\Location");

        $this->assertInstanceOf('App\Client', $location->client);
    }


    /** @test */
    function une_location_peut_generer_plusieurs_flux_financiers_apres_insertion_quand_la_periode_est_sur_plusieurs_mois()
    {
        
        $loc =create('App\Location',['date_dbt'=>"2020-10-12 11:00","date_fin_prev"=>"2020-12-12 11:00"]);
        $this->assertFalse($loc->durationOnSameMonth);
    }

    /** @test */
    function une_location_a_un_detail_journalier_sur_la_duree()
    {

        $location = create("App\Location",['date_dbt'=>"2019-12-12",'date_fin_prev'=>"2019-12-24"]);
        $details =$location->detailsFinanciersJournaliers()
                 ->reject(function ($value, $key)  {
                  return $value['date_fin_prev'] ==  Carbon::parse("2019-12-24")->addDay()->format('Y-m-d');
        });
        $this->assertCount(Carbon::parse("2019-12-24")->diffInDays(Carbon::parse("2019-12-12")),$details);

    }

    /** @test */
    function une_location_peut_s_etaler_sur_au_moins_2_different_ou_pas()
    {


        $now=Carbon::create(2019,10,10,0);
        $dbt = $now->subMonths(5)->toDateTimeString();
        $fin = $now->addMonth()->toDateTimeString();

        //given we have a rent
        $location = create("App\Location",['date_dbt'=>$dbt,'date_fin_prev'=>$fin]);

        $this->assertFalse($location->durationOnSameMonth);

        $now=Carbon::create(2019,10,10,0);
        $dbt = $now->subMonths(5)->toDateTimeString();
        $fin = $now->addDays(5)->toDateTimeString();

        //given we have a rent
        $location2 = create("App\Location",['date_dbt'=>$dbt,'date_fin_prev'=>$fin]);

        $this->assertTrue($location2->durationOnSameMonth);
    }

}
