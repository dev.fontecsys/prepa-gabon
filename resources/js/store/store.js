import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


import members from "./modules/members"
import params from "./modules/params"
import enterprises from "./modules/enterprises"
import projets from "./modules/projets"

export default  new Vuex.Store({
  modules:{
    members,params,enterprises,projets
  },
  strict:true,
  state: {
    count: 0,

    members:[]
  },
  getters:{

  },
  mutations: {
    increment (state) {
      state.count++
    },

  },
  methods:
  {

  },
  actions:
  {
    
  }
})