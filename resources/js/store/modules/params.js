
const state  = 
{
    projets:[],
    pays:[],
    formes_juridiques:["EI","SUARL/EURL","SARL","SAS","SASU","SA","SNC","SCS","GIE","Autre","SP","SCI"],
    civilites:['m.','mme','mlle','dr','pr'],
    statuts_maritaux:['célibataire','marié(e)','veuf(ve)','concubinage'],
    secteurs:[],
    formations:[],
    enterprises:[],
    promotions:[],
    competences:[],
}; 

const getters  = {
         
      _pays: state => {
        return state.pays
      },
      _enterprises: state => {
        return state.enterprises
      },
      _projets: state => {
        return state.projets
      },
      _formations: state => {
        return state.formations
      }
      ,
      _competences: state => {
        return state.competences
      },
      _formes_juridiques: state => {
        return state.formes_juridiques
      },
      _civilites: state => {
        return state.civilites
      },
      _statuts_maritaux: state => {
        return state.statuts_maritaux
      },
      _secteurs: state => {
        return state.secteurs
      },
      _promotions: state => {
        var currentYear = moment().year();
        var lastYear = 1950;
        var promotions = [];
           for(var i = currentYear ; i > lastYear ; i-- )
           {
               promotions.push(i);
           }
  
         return promotions
      }
};

const actions = 
{
    async loadOptions({commit})
    {
      
      await axios.get("/api/dispatcher/options/all").then((response) =>
      {

        commit('SET_PROJETS',response.data.projets)
        commit('SET_PAYS',response.data.pays)
        commit('SET_FORMATIONS',response.data.formations)
        commit('SET_SECTEURS',response.data.secteurs)
        commit('SET_COMPETENCES',response.data.competences)
        commit('SET_ENTERPRISES',response.data.enterprises) 

      },
      (error) =>
      {
        console.log(error)
      });
    },
}

const mutations = 
{
    SET_PAYS(state,payload)
    {
         state.pays = payload
    },
    SET_PROJETS(state,payload)
    {
         state.projets = payload
    },
    SET_FORMATIONS(state,payload)
    {
         state.formations = payload
    },
    SET_SECTEURS(state,payload)
    {
         state.secteurs = payload
    },
    SET_COMPETENCES(state,payload)
    {
         state.competences = payload
    },
    
    SET_PROMOTIONS(state,payload)
    {
         state.promotions = payload
    },
    SET_PROMOTIONS(state,payload)
    {
         state.promotions = payload
    },

    SET_ENTERPRISES(state,payload)
    {
         state.enterprises = payload
    }
}

export default 
{
    namespaced : true,
    state,getters,actions,mutations
}