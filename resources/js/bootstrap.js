window.Vue = require('vue')
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
    require('admin-lte');
} catch (e) {}

Array.prototype.sum = function (prop) {
    var total = 0
    for ( var i = 0, _len = this.length; i < _len; i++ ) {
        total += this[i][prop]
    }
    return total
}
window.toHex = function() {
  // storing all letter and digit combinations 
    // for html color code 
    var letters = "0123456789ABCDEF"; 
  
    // html color code starts with # 
    var color = '#'; 
  
    // generating 6 times as HTML color code consist 
    // of 6 letter or digits 
    for (var i = 0; i < 6; i++) 
       color += letters[(Math.floor(Math.random() * 16))]; 
    return color;
}
//MOment
window.moment= require('moment');
moment.locale('fr', {
    months : 'janvier_février_mars_avril_mai_juin_juillet_août_septembre_octobre_novembre_décembre'.split('_'),
    monthsShort : 'janv._févr._mars_avr._mai_juin_juil._août_sept._oct._nov._déc.'.split('_'),
    monthsParseExact : true,
    weekdays : 'dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi'.split('_'),
    weekdaysShort : 'dim._lun._mar._mer._jeu._ven._sam.'.split('_'),
    weekdaysMin : 'Di_Lu_Ma_Me_Je_Ve_Sa'.split('_'),
    weekdaysParseExact : true,
    longDateFormat : {
        LT : 'HH:mm',
        LTS : 'HH:mm:ss',
        L : 'DD/MM/YYYY',
        LL : 'D MMMM YYYY',
        LLL : 'D MMMM YYYY HH:mm',
        LLLL : 'dddd D MMMM YYYY HH:mm'
    },
    calendar : {
        sameDay : '[Aujourd’hui à] LT',
        nextDay : '[Demain à] LT',
        nextWeek : 'dddd [à] LT',
        lastDay : '[Hier à] LT',
        lastWeek : 'dddd [dernier à] LT',
        sameElse : 'L'
    },
    relativeTime : {
        future : 'dans %s',
        past : 'il y a %s',
        s : 'quelques secondes',
        m : 'une minute',
        mm : '%d minutes',
        h : 'une heure',
        hh : '%d heures',
        d : 'un jour',
        dd : '%d jours',
        M : 'un mois',
        MM : '%d mois',
        y : 'un an',
        yy : '%d ans'
    },
    dayOfMonthOrdinalParse : /\d{1,2}(er|e)/,
    ordinal : function (number) {
        return number + (number === 1 ? 'er' : 'e');
    },
    meridiemParse : /PD|MD/,
    isPM : function (input) {
        return input.charAt(0) === 'M';
    },
    // In case the meridiem units are not separated around 12, then implement
    // this function (look at locale/id.js for an example).
    // meridiemHour : function (hour, meridiem) {
    //     return /* 0-23 hour, given meridiem token and hour 1-12 */ ;
    // },
    meridiem : function (hours, minutes, isLower) {
        return hours < 12 ? 'PD' : 'MD';
    },
    week : {
        dow : 1, // Monday is the first day of the week.
        doy : 4  // Used to determine first week of the year.
    }
});

window.numeral= require('numeral');
window.toDateFormat = function(date_frmt,frmt)
{
    return moment(date_frmt).format(frmt)
}

//AUTHORIZATIONS
Vue.prototype.signedIn = window.App ? window.App.signedIn : false ;


let authorizations = require('./authorizations');

Vue.prototype.authorize = function (...params) {
    if (! window.App.signedIn) return false;

    if (params[0] == 'can') {
        return authorizations.hasAbility(params[1]);
    }
    else if (params[0] == 'any') 
    {
        if(Array.isArray(params[1]))
        return authorizations.hasAnyAbility(params[1]);
    }
    else if (params[0] == 'is') {
        return authorizations.is(params[1]);
    }
    else if (params[0]!=undefined && params[0]!=null && params[0]!="" && params[0] != 'is' && params[0] != 'can' ) {
        return authorizations.hasAbility(params[0]);
    }

    return false
};
Vue.prototype.authorizeAny = function (...params) {
    if (! window.App.signedIn) return false;

    if (params[0] == 'can') {
        return authorizations.hasAbility(params[1]);
        var check = false;
        
    }
    else if (params[0] == 'is') {
        return authorizations.is(params[1]);
    }
    else if (params[0]!=undefined && params[0]!=null && params[0]!="" && params[0] != 'is' && params[0] != 'can' ) {
        return authorizations.hasAbility(params[0]);
    }

    return false
};

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN': window.App.csrfToken,
    'Accept' : 'application/json',
};

// let csrf_token = document.head.querySelector('meta[name="csrf-token"]');
// if (csrf_token) {
//     window.axios.defaults.headers.common['X-CSRF-TOKEN'] =csrf_token.content;
//   }
// let api_token = document.head.querySelector('meta[name="api-token"]');

// if (api_token) {
//   window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + api_token.content;
// }


//Events

window.event = new Vue();

window.monnaie= function(value)
{
   return  numeral(value).format('0,0');
}
window.flash = function(message,type="success")
{
    window.event.$emit("flash",{message : message ,type : type});
}

window.log = function(message)
{
    console.log(message)
}
window.blockUI = require('./plugins/BlockUI.js');
$.blockUI.defaults.css = {}

import jc from 'jquery-confirm';

window.onAlert = function(message,title="Information")
{
    $.alert({
        closeIcon: true,
        closeIconClass: 'fas fa-close',
        title: title,
        theme: 'light',
        animateFromElement: false,
        type: 'blue',
        content: message,
    });
}
window.alertConfirm  = function (message="Vous ne serez pas en mesure de le changer!",title="Voulez-vous supprimer cet élément ?") {
    return new Promise((resolve, reject) =>
    {
        $.confirm
        ({
            closeIcon: true,
            closeIconClass: 'fas fa-close',
            title: title,
            theme: 'light',
            animateFromElement: false,
            type: 'red',
            content: message,
            buttons: {
                tryAgain: {
                    text: 'Confirmer',
                    btnClass: 'btn-red',
                    action: function(){
                           resolve(true)
                    }
                },
                close: {
                    text: 'Annuler',
                    btnClass: 'btn-default',
                    close: function(){
                        resolve(false)
                    }
                }
            }
        });
    });
}
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
Number.prototype.pad = function(n) {
    return new Array(n).join('0').slice((n || 2) * -1) + this;
}
String.prototype.ucFirst = function(n) {
    return new n.charAt(0).toUpperCase() + n.slice(1)
}

String.prototype.equals = function(n,param) {
    return n==param ? true : false
}



//Verifie si une variable 
window.isDefined = function(elem)
{
    if(elem!= null &&  elem != undefined && elem!="")
     return true 
    else 
     return false
}