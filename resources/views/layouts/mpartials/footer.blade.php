<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Made by <b class="text-uppercase">fontecsys</b>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2020.</strong>Tous droits reservés.
</footer>
  